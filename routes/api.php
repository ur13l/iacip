<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->middleware('cors')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'usuario', 'middleware' => 'cors'], function() {
    Route::post('crear', 'UserController@crear');
    Route::middleware('auth.admin')->post('usuariosactivos', 'UserController@usuariosactivos');
    Route::middleware('auth.admin')->post('getusuario', 'UserController@getUsuario');
    Route::middleware('auth.api')->post('actualizar', 'UserController@actualizar');
    Route::post('forgotpassword', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('resetpassword', 'Auth\ResetPasswordController@reset');
    Route::post('login', 'UserController@login');
});

Route::group(['prefix' => 'novedades', 'middleware'=>['auth.admin', 'cors']], function() {
    Route::post('crear', 'NovedadesController@crear');
    Route::post('actualizar', 'NovedadesController@actualizar');
    Route::post('eliminar', 'NovedadesController@eliminar');
});
Route::group(['prefix' => 'novedades', 'middleware'=>['cors']], function() {
    Route::post('get', 'NovedadesController@get');
});

Route::group(['prefix' => 'boletin', 'middleware'=>['auth.admin', 'cors']], function() {
    Route::post('crear', 'BoletinController@crear');
    Route::post('actualizar', 'BoletinController@actualizar');
    Route::post('eliminar', 'BoletinController@eliminar');
});

Route::group(['prefix' => 'boletin', 'middleware'=>['cors']], function() {
    Route::post('get', 'BoletinController@get');
    Route::post('/', 'BoletinController@boletin');
});

Route::group(['prefix' => 'robligacion', 'middleware'=>['auth.admin', 'cors']], function() {
    Route::post('crear', 'RecursoObligacionController@crear');
    Route::post('actualizar', 'RecursoObligacionController@actualizar');
    Route::post('eliminar', 'RecursoObligacionController@eliminar');
});

Route::group(['prefix' => 'robligacion', 'middleware'=>['cors']], function() {
    Route::post('get', 'RecursoObligacionController@get');
});

Route::group(['prefix' => 'mensaje', 'middleware'=>['auth.api', 'cors']], function() {
    Route::post('crear', 'MensajeController@crear');
    Route::post('mensajesfrom', 'MensajeController@mensajesFrom');
    Route::post('mensajesto', 'MensajeController@mensajesTo');
    Route::post('mensajes', 'MensajeController@mensajes');
    Route::middleware('auth.admin')->post('usuariosmensajes', 'MensajeController@usuariosMensajes');
});

Route::group(['prefix' => 'peticion', 'middleware'=>['auth.admin', 'cors']], function() {
    Route::post('crear', 'PeticionController@crear');
    Route::post('actualizar', 'PeticionController@actualizar');
    Route::post('eliminar', 'PeticionController@eliminar');
    Route::post('get', 'PeticionController@get');
    Route::post('/', 'PeticionController@peticion');
});

Route::group(['prefix' => 'peticion', 'middleware'=>['auth.api','cors']], function() {
    Route::post('peticiones', 'PeticionController@peticiones');
});

Route::group(['prefix' => 'recursopeticion', 'middleware'=>['auth.admin', 'cors']], function() {
    Route::post('crear', 'RecursoPeticionController@crear');
    Route::post('actualizar', 'RecursoPeticionController@actualizar');
    Route::post('eliminar', 'RecursoPeticionController@eliminar');
    Route::post('get', 'RecursoPeticionController@get');
    Route::post('/', 'RecursoPeticionController@recurso');
});

Route::group(['prefix' => 'pusher', 'middleware'=>['auth.api','cors']], function() {
    Route::post('auth', 'MensajeController@pusherAuth');
});

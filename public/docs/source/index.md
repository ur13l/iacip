---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)
<!-- END_INFO -->

#general
<!-- START_de20771152c4e4085184fc0101a363e8 -->
## Usuario: Crear
params: [email*, password*, nombre*, apepat*, apemat, telefono].

Método para la creación de usuarios.

> Example request:

```bash
curl -X POST "http://localhost/api/usuario/crear" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/usuario/crear",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/usuario/crear`


<!-- END_de20771152c4e4085184fc0101a363e8 -->

<!-- START_352123aa868a4b823c7fa1fcc83b64a6 -->
## Usuario: Ver Usuarios Activos
params: [api_token*].

Método que devuelve la lista de todos los usuarios activos.

> Example request:

```bash
curl -X POST "http://localhost/api/usuario/usuariosactivos" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/usuario/usuariosactivos",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/usuario/usuariosactivos`


<!-- END_352123aa868a4b823c7fa1fcc83b64a6 -->

<!-- START_2680f310ed85adc0f6fa36feda067191 -->
## Usuario: Obtener usuario por email
params: [api_token*, email*].

ADMIN: Servicio que devuelve la información de un usuario utilizando su email como parámetro

> Example request:

```bash
curl -X POST "http://localhost/api/usuario/getusuario" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/usuario/getusuario",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/usuario/getusuario`


<!-- END_2680f310ed85adc0f6fa36feda067191 -->

<!-- START_7e0e32ad6de740d9f793a03381e954c0 -->
## Usuario: Actualizar
params: [api_token*, email, nombre, apepat, apemat, telefono].

Método para actualizar los datos de un usuario.

> Example request:

```bash
curl -X POST "http://localhost/api/usuario/actualizar" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/usuario/actualizar",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/usuario/actualizar`


<!-- END_7e0e32ad6de740d9f793a03381e954c0 -->

<!-- START_eff6330f55157f00dac34f9899c47bf7 -->
## Usuario: Olvidaste tu contraseña
params: [email*].

Método que envía un email al usuario con un enlace para recuperar la contraseña.

> Example request:

```bash
curl -X POST "http://localhost/api/usuario/forgotpassword" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/usuario/forgotpassword",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/usuario/forgotpassword`


<!-- END_eff6330f55157f00dac34f9899c47bf7 -->

<!-- START_e57037d2adc662642776291991086582 -->
## Usuario: Restaurar contraseña
params: [token*, email*, password*, password_confirmation*].

Método que reestablece la contraseña.

> Example request:

```bash
curl -X POST "http://localhost/api/usuario/resetpassword" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/usuario/resetpassword",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/usuario/resetpassword`


<!-- END_e57037d2adc662642776291991086582 -->

<!-- START_28921708e29ff4bf510fcd50c5f0a3a9 -->
## Usuario: Login
params: [email*, password*].

Método para iniciar sesión con email y password.

> Example request:

```bash
curl -X POST "http://localhost/api/usuario/login" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/usuario/login",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/usuario/login`


<!-- END_28921708e29ff4bf510fcd50c5f0a3a9 -->

<!-- START_f1722af115371ca0be809e872613bdaf -->
## Novedades: Crear
params: [titulo*, api_token*, descripcion, imagen, status].

Método para la creación de novedades.

> Example request:

```bash
curl -X POST "http://localhost/api/novedades/crear" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/novedades/crear",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/novedades/crear`


<!-- END_f1722af115371ca0be809e872613bdaf -->

<!-- START_9020d113172b4743cc460e3d2aace0d2 -->
## Novedades: Actualizar
params: [id*, api_token*, titulo, descripcion, imagen, status].

Método para actualizar los datos de una novedad.

> Example request:

```bash
curl -X POST "http://localhost/api/novedades/actualizar" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/novedades/actualizar",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/novedades/actualizar`


<!-- END_9020d113172b4743cc460e3d2aace0d2 -->

<!-- START_2aed1374e57ecb29667b1e69b8bd4705 -->
## Novedades: Eliminar novedad
params: [api_token*, id*].

Método que elimina una novedad a partir de un id.

> Example request:

```bash
curl -X POST "http://localhost/api/novedades/eliminar" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/novedades/eliminar",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/novedades/eliminar`


<!-- END_2aed1374e57ecb29667b1e69b8bd4705 -->

<!-- START_6ec89eb8c5d49de4c9fb267844a67b72 -->
## Novedades: Ver todas las novedades
params: [].

Método que devuelve la lista de todas las novedades.

> Example request:

```bash
curl -X POST "http://localhost/api/novedades/get" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/novedades/get",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/novedades/get`


<!-- END_6ec89eb8c5d49de4c9fb267844a67b72 -->

<!-- START_cc5a872dff7c5d0cb9b22a281d933391 -->
## Boletín: Crear
params: [titulo*, api_token*, descripcion, imagen, status].

Método para la creación de boletines.

> Example request:

```bash
curl -X POST "http://localhost/api/boletin/crear" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/boletin/crear",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/boletin/crear`


<!-- END_cc5a872dff7c5d0cb9b22a281d933391 -->

<!-- START_4f7cc65a3f0e7846b4a46503cc8babf7 -->
## Boletín: Actualizar
params: [id*, api_token*, titulo, descripcion, imagen, status].

Método para actualizar los datos de un boletín.

> Example request:

```bash
curl -X POST "http://localhost/api/boletin/actualizar" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/boletin/actualizar",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/boletin/actualizar`


<!-- END_4f7cc65a3f0e7846b4a46503cc8babf7 -->

<!-- START_ce816127a4baeb31b81ccb94a01b94e7 -->
## Boletin: Eliminar boletín
params: [api_token*, id*].

Método que elimina un boletín a partir de un id.

> Example request:

```bash
curl -X POST "http://localhost/api/boletin/eliminar" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/boletin/eliminar",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/boletin/eliminar`


<!-- END_ce816127a4baeb31b81ccb94a01b94e7 -->

<!-- START_3eaf19fdfb50728b96891603585879ea -->
## Boletín: Ver todos los boletines
params: [].

Método que devuelve la lista de todos los boletines

> Example request:

```bash
curl -X POST "http://localhost/api/boletin/get" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/boletin/get",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/boletin/get`


<!-- END_3eaf19fdfb50728b96891603585879ea -->

<!-- START_60de5e788380a643f81881458784efc2 -->
## Boletín: Ver un boletín acorde a un id enviado
params: [id].

Método que devuelve el boletín solicitado mediante su id.

> Example request:

```bash
curl -X POST "http://localhost/api/boletin" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/boletin",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/boletin`


<!-- END_60de5e788380a643f81881458784efc2 -->

<!-- START_4b1e239cc4fbd6664dcf492619cf8de9 -->
## Recurso Obligación: Crear
params: [api_token*, id_obligacion*, descripcion, archivo, status].

Método para la creación de un recurso de obligación.

> Example request:

```bash
curl -X POST "http://localhost/api/robligacion/crear" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/robligacion/crear",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/robligacion/crear`


<!-- END_4b1e239cc4fbd6664dcf492619cf8de9 -->

<!-- START_0e50749865764dfc65a420e4a771e55e -->
## Recurso Obligación: Actualizar
params: [id*, api_token*, descripcion, archivo, status].

Método para actualizar los datos de un recurso de obligación.

> Example request:

```bash
curl -X POST "http://localhost/api/robligacion/actualizar" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/robligacion/actualizar",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/robligacion/actualizar`


<!-- END_0e50749865764dfc65a420e4a771e55e -->

<!-- START_1dc64c48e3ce489223070e2222d9119d -->
## Recurso obligación: Eliminar
params: [api_token*, id*].

Método que elimina un recurso de obligación a partir de un id.

> Example request:

```bash
curl -X POST "http://localhost/api/robligacion/eliminar" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/robligacion/eliminar",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/robligacion/eliminar`


<!-- END_1dc64c48e3ce489223070e2222d9119d -->

<!-- START_bc6f46808bef37461322bb10ea79edde -->
## Recurso Obligación: Listado de recursos de obligación.

params: [id_obligacion*].
Ver todos los recursos de obligación dado el id de una obligación

> Example request:

```bash
curl -X POST "http://localhost/api/robligacion/get" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/robligacion/get",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/robligacion/get`


<!-- END_bc6f46808bef37461322bb10ea79edde -->

<!-- START_2e43a673b36313563abe9b5eadf438fb -->
## Mensaje: Crear
params: [api_token*, to*, descripcion*].

Método para la creación de un mensaje

> Example request:

```bash
curl -X POST "http://localhost/api/mensaje/crear" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/mensaje/crear",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/mensaje/crear`


<!-- END_2e43a673b36313563abe9b5eadf438fb -->

<!-- START_7eae64efdf885f12ada0f94f799cfbcf -->
## Mensaje: Mensajes From
params: [api_token*, from*].

Método para devolver la lista de mensajes a partir del emisor.

> Example request:

```bash
curl -X POST "http://localhost/api/mensaje/mensajesfrom" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/mensaje/mensajesfrom",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/mensaje/mensajesfrom`


<!-- END_7eae64efdf885f12ada0f94f799cfbcf -->

<!-- START_7adcd3cb83ec0543fa09883909305e73 -->
## Mensaje: Mensajes To
params: [api_token*, to*].

Método para devolver la lista de mensajes a partir del receptor.

> Example request:

```bash
curl -X POST "http://localhost/api/mensaje/mensajesto" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/mensaje/mensajesto",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/mensaje/mensajesto`


<!-- END_7adcd3cb83ec0543fa09883909305e73 -->

<!-- START_f06195fbf3ee535ea6aa49b6c2ec258a -->
## Mensaje: Mensajes
params: [api_token*, id_usuario*].

Método para devolver la lista de mensajes enviados y recibidos entre el usuario y otro usuario.

> Example request:

```bash
curl -X POST "http://localhost/api/mensaje/mensajes" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/mensaje/mensajes",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/mensaje/mensajes`


<!-- END_f06195fbf3ee535ea6aa49b6c2ec258a -->

<!-- START_53f09bcbecad6c59e0db06b44e7a9cd5 -->
## Mensaje: Usuarios que han enviado mensajes
params: [api_token*]
Devuelve sin repetir a los usuarios que han enviado un mensaje alguna vez.

> Example request:

```bash
curl -X POST "http://localhost/api/mensaje/usuariosmensajes" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/mensaje/usuariosmensajes",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/mensaje/usuariosmensajes`


<!-- END_53f09bcbecad6c59e0db06b44e7a9cd5 -->

<!-- START_9776b5b12b7d725e537a345142834270 -->
## Petición: Crear
params: [api_token*, folio*, id_usuario*, descripcion*, fecha_inicio*, status].

Método para la creación de peticiones.

> Example request:

```bash
curl -X POST "http://localhost/api/peticion/crear" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/peticion/crear",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/peticion/crear`


<!-- END_9776b5b12b7d725e537a345142834270 -->

<!-- START_e2048acfb1b243ac4f78f8090b10c6e3 -->
## Petición: Actualizar
params: [folio*, api_token*, descripcion, fecha_inicio, status].

Método para actualizar los datos de una petición.

> Example request:

```bash
curl -X POST "http://localhost/api/peticion/actualizar" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/peticion/actualizar",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/peticion/actualizar`


<!-- END_e2048acfb1b243ac4f78f8090b10c6e3 -->

<!-- START_f23a07f8ee912653f85d6a5421411cdc -->
## Petición: Eliminar
params: [api_token*, folio*].

Método que elimina una petición a partir de un id.

> Example request:

```bash
curl -X POST "http://localhost/api/peticion/eliminar" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/peticion/eliminar",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/peticion/eliminar`


<!-- END_f23a07f8ee912653f85d6a5421411cdc -->

<!-- START_4a49cc8a0830ee014f2b2e42a2e74c98 -->
## Peticion: Ver todas las peticiones
params: [api_token*].

Método que devuelve la lista de todas las peticiones

> Example request:

```bash
curl -X POST "http://localhost/api/peticion/get" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/peticion/get",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/peticion/get`


<!-- END_4a49cc8a0830ee014f2b2e42a2e74c98 -->

<!-- START_5110557ad4ce84f2afae8870d1b758f8 -->
## Petición: Ver una petición acorde a un id enviado
params: [api_token*, folio*].

Método que devuelve la petición solicitado mediante su id.

> Example request:

```bash
curl -X POST "http://localhost/api/peticion" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/peticion",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/peticion`


<!-- END_5110557ad4ce84f2afae8870d1b758f8 -->

<!-- START_4b9d87b28477ea5b2d367412fa5825e6 -->
## Petición: Peticiones por usuario
params: [api_token*].

Devuelve una lista de peticiones de acuerdo al api_token del usuario.

> Example request:

```bash
curl -X POST "http://localhost/api/peticion/peticiones" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/peticion/peticiones",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/peticion/peticiones`


<!-- END_4b9d87b28477ea5b2d367412fa5825e6 -->

<!-- START_a5f0fc51383b04be402e27480a67a84f -->
## Recurso Petición: Crear
params: [api_token*, folio*, descripcion*, archivo*, status*].

Método para la creación de recursos de peticion.

> Example request:

```bash
curl -X POST "http://localhost/api/recursopeticion/crear" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/recursopeticion/crear",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/recursopeticion/crear`


<!-- END_a5f0fc51383b04be402e27480a67a84f -->

<!-- START_c464e42eb36a06c19843c6e4040496a9 -->
## Recurso Petición: Actualizar
params: [id*, api_token*, descripcion, archivo, fecha, status].

Método para actualizar los datos de un recurso de petición.

> Example request:

```bash
curl -X POST "http://localhost/api/recursopeticion/actualizar" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/recursopeticion/actualizar",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/recursopeticion/actualizar`


<!-- END_c464e42eb36a06c19843c6e4040496a9 -->

<!-- START_242c7f17639285ac47cc5ec554be084c -->
## Recurso Petición: Eliminar
params: [api_token*, id*].

Método que elimina un recurso de petición a partir de un id.

> Example request:

```bash
curl -X POST "http://localhost/api/recursopeticion/eliminar" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/recursopeticion/eliminar",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/recursopeticion/eliminar`


<!-- END_242c7f17639285ac47cc5ec554be084c -->

<!-- START_2473e5271d4d3314223c44e5333d1646 -->
## Recurso Peticion: Ver todos los recursos de peticiones
params: [api_token*].

Método que devuelve la lista de todos los recursos

> Example request:

```bash
curl -X POST "http://localhost/api/recursopeticion/get" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/recursopeticion/get",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/recursopeticion/get`


<!-- END_2473e5271d4d3314223c44e5333d1646 -->

<!-- START_ef320991e381aeddfd0b4a1d260bce58 -->
## Recurso Petición: Ver un recurso acorde a un id enviado
params: [api_token*, id*].

Método que devuelve el recurso de petición solicitado mediante su id.

> Example request:

```bash
curl -X POST "http://localhost/api/recursopeticion" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/recursopeticion",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/recursopeticion`


<!-- END_ef320991e381aeddfd0b4a1d260bce58 -->


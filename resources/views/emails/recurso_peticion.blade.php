<p>El suscrito licenciado <strong>Diego Paul Muñiz Delgado</strong>: Con el carácter de Actuario, nombrado por el pleno del  Instituto de Acceso a la Información Pública para el Estado de Guanajuato, en la Primera 
Sesión Ordinaria del Décimo Segundo año de ejercicio.
	</p>
	<p>Por medio del presente, le notifico a la cuenta de correo electrónico señalada para tal efecto, en su recurso de revisión, radicado bajo el número <strong>{{$peticion->folio}}</strong>, el auto de <strong>fecha {{$fecha}}</strong>, recaído al medio de impugnación aludido; mismo que encontrará anexo al presente en archivo generado de manera electrónica,  en formato “PDF”; lo 
anterior en ejercicio de las atribuciones que me confiere el artículo 185 de la Ley de Transparencia y Acceso a la Información Pública para el Estado de Guanajuato. 
	</p>
	<p>
	Agradeciendo de antemano acuse de recibido el presente correo, que sirve de notificación para todos los efectos legales a que haya lugar.</p>

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $id_obligacion
 * @property string $descripcion
 * @property string $url
 * @property boolean $status
 * @property string $fecha
 * @property string $created_at
 * @property string $updated_at
 * @property Obligacion $obligacion
 */
class RecursoObligacion extends Model
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'recurso_obligacion';

    /**
     * @var array
     */
    protected $fillable = ['id_obligacion', 'descripcion', 'url', 'status', 'fecha', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function obligacion()
    {
        return $this->belongsTo('App\Obligacion', 'id_obligacion');
    }
}

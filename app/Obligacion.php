<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property boolean $status
 * @property string $url
 * @property string $created_at
 * @property string $updated_at
 * @property RecursoObligacion[] $recursoObligacions
 */
class Obligacion extends Model
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'obligacion';

    /**
     * @var array
     */
    protected $fillable = ['nombre', 'status', 'url', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function recursosObligacion()
    {
        return $this->hasMany('App\RecursoObligacion', 'id_obligacion');
    }
}

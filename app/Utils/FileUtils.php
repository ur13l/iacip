<?php
namespace App\Utils;

class FileUtils {

    /**
    * Método para guardar una imagen con el archivo, una ruta de destino y el prefijo de la imagen
    */
    public static function guardar($file, $destinationPath){
        if ($file->isValid()) { 
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $fileName = FileUtils::checkName($destinationPath, $file->getClientOriginalName()); // renameing image
            $file->move($destinationPath, $fileName); // uploading file to given path
            return url($destinationPath . $fileName);
        }
        return null;
    }

    /**
     * Método que permite que los nombres de un archivo no se repitan.
     *
     * @param [type] $name
     * @return void
     */
    public static function checkName($path, $fileName) {
        $p = $path . $fileName;
        if(file_exists($p)){
            return FileUtils::checkName($path,"_" . $fileName );
        }
        return $fileName;
    }


    /**
     * Eliminar imagen
     * Método estático que permite la eliminación de una imagen si esta existe dentro del servidor.
     * @param $url
     */
    public static function eliminar($url){
        $spl = explode("/", $url);
        if(count($spl) > 4) {
            $path = $spl[count($spl) - 4] . "/" . $spl[count($spl) - 3] . "/" . $spl[count($spl) - 2] . "/" . $spl[count($spl) - 1];
            if (file_exists($path))
                unlink($path);
        }
    }
}
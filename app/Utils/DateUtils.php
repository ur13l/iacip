<?php
namespace App\Utils;

class DateUtils {
    
    const DIAS = array(
        '1' => 'uno',
        '2' => 'dos',
        '3' => 'tres',
        '4' => 'cuatro',
        '5' => 'cinco',
        '6' => 'seis',
        '7' => 'siete',
        '8' => 'ocho',
        '9' => 'nueve',
        '10' => 'diez',
        '11' => 'once',
        '12' => 'doce',
        '13' => 'trece',
        '14' => 'catorce',
        '15' => 'quince',
        '16' => 'dieciséis',
        '17' => 'diecisiete',
        '18' => 'dieciocho',
        '19' => 'diecinueve',
        '20' => 'veinte',
        '21' => 'veintiuno',
        '22' => 'veintidós',
        '23' => 'veintitrés',
        '24' => 'veinticuatro',
        '25' => 'veinticinco',
        '26' => 'veintiséis',
        '27' => 'veintisiete',
        '28' => 'veintiocho',
        '29' => 'veintinueve',
        '30' => 'treinta',
        '31' => 'treinta y uno'
    );

    const MESES = array(
        '1' => 'enero',
        '2' => 'febrero',
        '3' => 'marzo',
        '4' => 'abril',
        '5' => 'mayo',
        '6' => 'junio',
        '7' => 'julio',
        '8' => 'agosto',
        '9' => 'septiembre',
        '10' => 'octubre',
        '11' => 'noviembre',
        '12' => 'diciembre'
    );

    public static function convertirFechaContrato($date) {
        return $date->day . " " . constant('self::DIAS')[$date->day . ""] . " de " . 
        constant('self::MESES')[$date->month . ""] . " de " . $date->year . " dos mil " . 
        constant('self::DIAS')[substr($date->year . "", 2)];
    }

    
}
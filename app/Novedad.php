<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property string $fecha
 * @property string $titulo
 * @property string $descripcion
 * @property string $url_foto
 * @property boolean $status
 * @property string $created_at
 * @property string $updated_at
 */
class Novedad extends Model
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'novedades';

    /**
     * @var array
     */
    protected $fillable = ['fecha', 'titulo', 'descripcion', 'url_foto', 'status', 'created_at', 'updated_at'];

}

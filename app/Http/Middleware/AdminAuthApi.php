<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AdminAuthApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('api')->check()) {
            $user = Auth::guard('api')->user();
            if($user->id_tipo == 1) {
                return $next($request);
            }
            
        }

        return response()->json(array(
            "status" => 500,
            "success" => false,
            "errors" => ["Usuario no autorizado. Función habilitada solo para administradores"],
            "data" => false
        ));
    }
}

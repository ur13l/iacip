<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Boletin;
use App\Utils\FileUtils;
use Validator; 
use Carbon\Carbon;

class BoletinController extends Controller
{
    
    /**
     * Boletín: Crear
     * params: [titulo*, api_token*, descripcion, imagen, status].
     * Método para la creación de boletines.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function crear(Request $request) {
        $errors = [];
        $data = null;
        $reglas = [
            'titulo' => 'required'
        ];

        $validacion = Validator::make($request->all(), $reglas);

        if ($validacion->fails()) {
            foreach ($validacion->errors()->all() as $error) {
                $errors[] = $error;
            }
        } else {
            $imagen = $request->file('imagen');
            //Cargado de imagen de un boletín
            $rutaImagen = FileUtils::guardar($request->file('imagen'), 'storage/boletin/saved/');

            $request->request->add(['url_foto' => $rutaImagen]);
            $request->request->add(['fecha' => Carbon::now('America/Mexico_City')]);
            $boletin = Boletin::create($request->all());
        
        }
        if (count($errors) > 0) {
            return response()->json([
                "success" => false,
                "errors" => $errors,
                "status" => 500,
                "data" => null
            ]);
        } else {
            return response()->json([
                "success" => true,
                "errors" => $errors,
                "status" => 200,
                "data" => $boletin
            ]);
        }
    }


    /**
     * Boletín: Ver todos los boletines
     * params: [].
     * Método que devuelve la lista de todos los boletines
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(Request $request) {
        $boletines = Boletin::all();
        return response()->json([
                "success" => true,
                "errors" => [],
                "status" => 200,
                "data" => $boletines
            ]);
    }

    /**
     * Boletín: Actualizar
     * params: [id*, api_token*, titulo, descripcion, imagen, status].
     * Método para actualizar los datos de un boletín.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function actualizar(Request $request) {
        $data = null;

        $boletin = Boletin::find($request->id);
        if($boletin) {
            if($request->file('imagen')) {
                //Cargado de imagen del boletín
                $rutaImagen = FileUtils::guardar($request->file('imagen'), 'storage/boletin/saved/');
                FileUtils::eliminar($boletin->url_foto);
                $request->request->add(['url_foto' => $rutaImagen]);
            }
            $boletin->update($request->all());
            $boletin->save();
            
            return response()->json([
                "success" => true,
                "errors" => [],
                "status" => 200,
                "data" => $boletin
            ]);
        }
        else {
            return response()->json([
                "success" => false,
                "errors" => ["Boletin no encontrado"],
                "status" => 404,
                "data" => null
            ]);
        }
        
    }

    /**
     * Boletin: Eliminar boletín
     * params: [api_token*, id*].
     * Método que elimina un boletín a partir de un id.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function eliminar(Request $request) {
        $boletin = Boletin::find($request->id);
        if($boletin) {
        $boletin->delete();
            return response()->json([
                    "success" => true,
                    "errors" => [],
                    "status" => 200,
                    "data" => true
                ]);
        }
        else {
             return response()->json([
                    "success" => false,
                    "errors" => ["El boletín no fue encontrado"],
                    "status" => 200,
                    "data" => false
                ]);
        }
    }


    /**
     * Boletín: Ver un boletín acorde a un id enviado
     * params: [id].
     * Método que devuelve el boletín solicitado mediante su id.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function boletin(Request $request) {
        $boletin = Boletin::find($request->id);
        if($boletin != null) {
            return response()->json([
            "success" => true,
            "errors" => [],
            "status" => 200,
            "data" => $boletin
            ]);
        }
        return response()->json([
            "success" => false,
            "errors" => ["El boletín no fue encontrado"],
            "status" => 404,
            "data" => false
        ]);
    }
}

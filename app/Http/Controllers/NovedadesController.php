<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Novedad;
use App\Utils\FileUtils;
use Validator; 
use Carbon\Carbon;

class NovedadesController extends Controller
{
    
    /**
     * Novedades: Crear
     * params: [titulo*, api_token*, descripcion, imagen, status].
     * Método para la creación de novedades.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function crear(Request $request) {
        $errors = [];
        $data = null;
        $reglas = [
            'titulo' => 'required'
        ];

        $validacion = Validator::make($request->all(), $reglas);

        if ($validacion->fails()) {
            foreach ($validacion->errors()->all() as $error) {
                $errors[] = $error;
            }
        } else {
            $imagen = $request->file('imagen');
            //Cargado de imagen de la novedad
            $rutaImagen = FileUtils::guardar($request->file('imagen'), 'storage/novedades/saved/');

            $request->request->add(['url_foto' => $rutaImagen]);
            $request->request->add(['fecha' => Carbon::now('America/Mexico_City')]);
            $novedad = Novedad::create($request->all());
        
        }
        if (count($errors) > 0) {
            return response()->json([
                "success" => false,
                "errors" => $errors,
                "status" => 500,
                "data" => $novedad
            ]);
        } else {
            return response()->json([
                "success" => true,
                "errors" => $errors,
                "status" => 200,
                "data" => $novedad
            ]);
        }
    }


    /**
     * Novedades: Ver todas las novedades
     * params: [].
     * Método que devuelve la lista de todas las novedades.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(Request $request) {
        $novedad = Novedad::all();
        return response()->json([
                "success" => true,
                "errors" => [],
                "status" => 200,
                "data" => $novedad
            ]);
    }

    /**
     * Novedades: Actualizar
     * params: [id*, api_token*, titulo, descripcion, imagen, status].
     * Método para actualizar los datos de una novedad.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function actualizar(Request $request) {
        $data = null;

        $novedad = Novedad::find($request->id);
        if($novedad) {
            if($request->file('imagen')) {
                //Cargado de imagen de la novedad
                $rutaImagen = FileUtils::guardar($request->file('imagen'), 'storage/novedades/saved/');
                FileUtils::eliminar($novedad->url_foto);
                $request->request->add(['url_foto' => $rutaImagen]);
            }
            $novedad->update($request->all());
            $novedad->save();
            
            return response()->json([
                "success" => true,
                "errors" => [],
                "status" => 200,
                "data" => $novedad
            ]);
        }
        else {
            return response()->json([
                "success" => false,
                "errors" => ["La novedad no fue encontrada"],
                "status" => 404,
                "data" => null
            ]);
        }
        
    }

    /**
     * Novedades: Eliminar novedad
     * params: [api_token*, id*].
     * Método que elimina una novedad a partir de un id.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function eliminar(Request $request) {
        $novedad = Novedad::find($request->id);
        if($novedad) {
            $novedad->delete();
            return response()->json([
                    "success" => true,
                    "errors" => [],
                    "status" => 200,
                    "data" => true
                ]);
        }
        else {
            return response()->json([
                    "success" => false,
                    "errors" => ["La novedad no fue encontrada"],
                    "status" => 404,
                    "data" => false
                ]);
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mensaje;
use Validator;
use Carbon\Carbon;
use Auth;
use App\User;
use App\Events\ChatEvent;
use App\Events\ChatToAdminEvent;

class MensajeController extends Controller
{
     /**
     * Mensaje: Crear
     * params: [api_token*, to*, descripcion*].
     * Método para la creación de un mensaje
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function crear(Request $request) {
        $errors = [];
        $data = null;
        $reglas = [
            'to' => 'required',
            'descripcion' => 'required'
        ];
        $mensaje = null;

        $validacion = Validator::make($request->all(), $reglas);

        if ($validacion->fails()) {
            foreach ($validacion->errors()->all() as $error) {
                $errors[] = $error;
            }
        } else {
            $user = Auth::guard('api')->user();
            $request->request->add(['from' => $user->id]);
            $request->request->add(['fecha' => Carbon::now('America/Mexico_City')]);
            $mensaje = Mensaje::create($request->all());
        
            if($user->isAdmin()){ 
                event(new \App\Events\ChatEvent($mensaje));
            }
            else {
                event(new \App\Events\ChatToAdminEvent($mensaje));
            }
        }
        if (count($errors) > 0) {
            return response()->json([
                "success" => false,
                "errors" => $errors,
                "status" => 500,
                "data" => $mensaje
            ]);
        } else {
            return response()->json([
                "success" => true,
                "errors" => $errors,
                "status" => 200,
                "data" => $mensaje
            ]);
        }
    }


     /**
     * Mensaje: Mensajes From
     * params: [api_token*, from*].
     * Método para devolver la lista de mensajes a partir del emisor.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function mensajesFrom(Request $request) {
        $mensajes = Mensaje::where('from', $request->from)->get();
        return response()->json([
                "success" => true,
                "errors" => [],
                "status" => 200,
                "data" => $mensajes
            ]);
    }

     /**
     * Mensaje: Mensajes To
     * params: [api_token*, to*].
     * Método para devolver la lista de mensajes a partir del receptor.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function mensajesTo(Request $request) {
         $mensajes = Mensaje::where('to', $request->to)->get();
        return response()->json([
                "success" => true,
                "errors" => [],
                "status" => 200,
                "data" => $mensajes
            ]);
    }


    /**
     * Mensaje: Usuarios que han enviado mensajes
     * params: [api_token*]
     * Devuelve sin repetir a los usuarios que han enviado un mensaje alguna vez.
     * @param Request $request
     * @return void
     */
    public function usuariosMensajes(Request $request) {
        $users = User::select(['users.id', 'users.nombre', 'users.apepat', 'users.apemat', 'users.email', 'mensaje.*'])->leftJoin('mensaje', function($q) {
            $q->on('users.id', '=', 'mensaje.from');
        })
            ->where('from', '!=', null)
            ->orderBy('mensaje.created_at', 'desc')
            ->get()->unique('email');
        return response()->json([
                "success" => true,
                "errors" => [],
                "status" => 200,
                "data" => $users
            ]);
    }

    /**
     * Mensaje: Mensajes 
     * params: [api_token*, id_usuario*].
     * Método para devolver la lista de mensajes enviados y recibidos entre el usuario y otro usuario.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function mensajes(Request $request) {
        $user = Auth::guard('api')->user();
        $mensajes = Mensaje::where(function($q) use ($user, $request) {
                $q->where('from', $user->id)
                    ->where('to', $request->id_usuario);
            })->orWhere(function ($q) use ($user, $request) {
                $q->where('to', $user->id)
                    ->where('from', $request->id_usuario);
            })->orderBy('created_at', 'desc')->get();
                
        return response()->json([
                "success" => true,
                "errors" => [],
                "status" => 200,
                "data" => $mensajes
            ]);
    }

    /**
     * Mensaje: Autenticación de Pusher 
     * params: [api_token*, id_usuario*].
     * Método para devolver la lista de mensajes enviados y recibidos entre el usuario y otro usuario.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function pusherAuth(Request $request) {
            return $this->pusher->socket_auth($request->get('channel_name'), $request->get('socket_id'));
          
    }
}

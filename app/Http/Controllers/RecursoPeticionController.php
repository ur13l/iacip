<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Validator;
use App\Peticion;
use App\RecursoPeticion;
use Carbon\Carbon;
use App\Utils\FileUtils;
use Mail;

class RecursoPeticionController extends Controller
{
     /**
     * Recurso Petición: Crear
     * params: [api_token*, folio*, descripcion*, archivo*, status*].
     * Método para la creación de recursos de peticion.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function crear(Request $request) {
        $errors = [];
        $data = null;
        $reglas = [
            'folio' => 'required',
            'descripcion' => 'required',
            'archivo' => 'required'
        ];

        $validacion = Validator::make($request->all(), $reglas);

        if ($validacion->fails()) {
            foreach ($validacion->errors()->all() as $error) {
                $errors[] = $error;
            }
        } else {
           $archivo = $request->file('archivo');
            //Cargado de archivo de recurso petición.

            $peticion = Peticion::where('folio', $request->folio)->first();
            
            if($peticion) {
                $file = $request->file('archivo');
                $path ='storage/rpeticion/'. $peticion->id .'/';
                $url = FileUtils::guardar($file, $path);
                $request->request->add(['url' => $url]);
                $request->request->add(['fecha' => Carbon::now('America/Mexico_City')]);
                $request->request->add(['id_peticion' => $peticion->id]);
                $recurso = RecursoPeticion::create($request->all());
                $fecha = \App\Utils\DateUtils::convertirFechaContrato($recurso->fecha);

                $user = $peticion->user;
                Mail::send('emails.recurso_peticion', ['user' => $user, 'fecha' => $fecha, 'peticion' => $peticion], function ($m) use ($user, $path, $recurso) {
                    $m->from('dmunizd@iacip-gto.org.mx', 'IACIP');
                    $m->attach($path . explode('/',$recurso->url)[count(explode('/', $recurso->url)) - 1]);
                    $m->to($user->email, $user->nombre)->subject('Recurso de petición generado');
                });
                
            }
            else {
                $errors[] = "La petición no existe";
            }
        }
        if (count($errors) > 0) {
            return response()->json([
                "success" => false,
                "errors" => $errors,
                "status" => 500,
                "data" => null
            ]);
        } else {
            return response()->json([
                "success" => true,
                "errors" => $errors,
                "status" => 200,
                "data" => $recurso
            ]);
        }
    }


    /**
     * Recurso Peticion: Ver todos los recursos de peticiones
     * params: [api_token*].
     * Método que devuelve la lista de todos los recursos
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(Request $request) {
        $recursos = RecursoPeticion::all();
        return response()->json([
                "success" => true,
                "errors" => [],
                "status" => 200,
                "data" => $recursos
            ]);
    }

    /**
     * Recurso Petición: Actualizar
     * params: [id*, api_token*, descripcion, archivo, fecha, status].
     * Método para actualizar los datos de un recurso de petición.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function actualizar(Request $request) {
        $data = null;

        $recurso = RecursoPeticion::find($request->id);
        if($recurso) {
            if($request->file('archivo')) {
                //Cargado de archivo
                $rutaImagen = FileUtils::guardar($request->file('archivo'), 'storage/rpeticion/' .$recurso->id_peticion .'/');
                FileUtils::eliminar($recurso->url);
                $request->request->add(['url' => $rutaImagen]);
            }
            $recurso->update($request->all());
            $recurso->save();
            
            return response()->json([
                "success" => true,
                "errors" => [],
                "status" => 200,
                "data" => $recurso
            ]);
        }
        else {
            return response()->json([
                "success" => false,
                "errors" => ["Petición no encontrada"],
                "status" => 404,
                "data" => null
            ]);
        }
        
    }

    /**
     * Recurso Petición: Eliminar
     * params: [api_token*, id*].
     * Método que elimina un recurso de petición a partir de un id.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function eliminar(Request $request) {
        $recurso = RecursoPeticion::find($request->id);
        if($recurso) {
        $recurso->delete();
            return response()->json([
                    "success" => true,
                    "errors" => [],
                    "status" => 200,
                    "data" => true
                ]);
        }
        else {
             return response()->json([
                    "success" => false,
                    "errors" => ["El recurso de petición no fue encontrado"],
                    "status" => 200,
                    "data" => false
                ]);
        }
    }


    /**
     * Recurso Petición: Ver un recurso acorde a un id enviado
     * params: [api_token*, id*].
     * Método que devuelve el recurso de petición solicitado mediante su id.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function recurso(Request $request) {
        $recurso = RecursoPeticion::find($request->id);
        if($recurso != null) {
            return response()->json([
            "success" => true,
            "errors" => [],
            "status" => 200,
            "data" => $recurso
            ]);
        }
        return response()->json([
            "success" => false,
            "errors" => ["El recurso de petición no fue encontrado"],
            "status" => 404,
            "data" => false
        ]);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;   
use Carbon\Carbon;
use App\User;

class UserController extends Controller
{
    
    
    /**
     * Usuario: Crear
     * params: [email*, password*, nombre*, apepat*, apemat, telefono].
     * Método para la creación de usuarios.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function crear(Request $request) {
        $errors = [];
        $data = null;
        $reglas = [
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'nombre' => 'required|string',
            'apepat' => 'required|string'
        ];

        $validacion = Validator::make($request->all(), $reglas);

        if ($validacion->fails()) {
            foreach ($validacion->errors()->all() as $error) {
                $errors[] = $error;
            }
        } else {
            $request->request->add(['id_tipo' => '2']);
            $usuario = User::create($request->all());
        
        }
        if (count($errors) > 0) {
            return response()->json([
                "success" => false,
                "errors" => $errors,
                "status" => 500,
                "data" => $data
            ]);
        } else {
            return response()->json([
                "success" => true,
                "errors" => $errors,
                "status" => 200,
                "data" => $usuario
            ]);
        }
    }


    /**
     * Usuario: Ver Usuarios Activos
     * params: [api_token*].
     * Método que devuelve la lista de todos los usuarios activos.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function usuariosactivos(Request $request) {
        $usuarios = User::where('id_tipo', '2')->select(['id', 'nombre', 'email', 'password', 'apepat', 
            'apemat', 'telefono'])->get();
        return response()->json([
                "success" => true,
                "errors" => [],
                "status" => 200,
                "data" => $usuarios
            ]);
    }

    /**
     * Usuario: Actualizar
     * params: [api_token*, email, nombre, apepat, apemat, telefono].
     * Método para actualizar los datos de un usuario.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function actualizar(Request $request) {
        $data = null;

        $usuario = Auth::guard('api')->user();
        $usuario->update($request->except('password'));
        $usuario->save();
        
        return response()->json([
            "success" => true,
            "errors" => [],
            "status" => 200,
            "data" => $usuario
        ]);
        
    }


    /**
     * Usuario: Login
     * params: [email*, password*].
     * Método para iniciar sesión con email y password.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
     public function login(Request $request) {
          if (Auth::once(['email' => $request->email, 'password' => $request->password])) {
                $usuario = Auth::user();
                return response()->json([
                    "success" => true,
                    "errors" => [],
                    "status" => 200,
                    "data" => $usuario
                ]);
            } else {
                return response()->json([
                    "success" => false,
                    "errors" => ["Usuario o contraseña incorrectos"],
                    "status" => 500,
                    "data" => []
                ]);
            }
     }


    /**
     * Usuario: Actualizar contraseña
     * params: [api_token*, email*, password*, password_confirmation*].
     * Permite cambiar la contraseña de un usuario con su confirmación.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
      public function actualizarContrasena(Request $request) {
          $errors = [];
          $usuario = null;
          $success = false;
          $status = 500;
          $reglas = [
            'email' => 'required|email',
            'password' => 'required|confirmed',
          ];

        $validacion = Validator::make($request->all(), $reglas);

        if ($validacion->fails()) {
            foreach ($validacion->errors()->all() as $error) {
                $errors[] = $error;
            }
        }
        else {
            $usuario = Auth::guard('api')->user();
          if($request->email != $usuario->email) {
            $errors[] = "El email no coincide con el api token de usuario";
            $usuario = null;
          }
          else {
            $usuario->password = $request->password;
            $usuario->save();
            $success = true;
            $status = 200;
          }     
        }

        return response()->json([
                "success" => $success,
                "errors" => $errors,
                "status" => $status,
                "data" => $usuario
            ]);
        
      }


       /**
     * Usuario: Obtener usuario por email
     * params: [api_token*, email*].
     * ADMIN: Servicio que devuelve la información de un usuario utilizando su email como parámetro
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
      public function getUsuario(Request $request) {
          $errors = [];
          $usuario = null;
          $success = false;
          $status = 500;
          $reglas = [
            'email' => 'required|email'
          ];

        $validacion = Validator::make($request->all(), $reglas);

        if ($validacion->fails()) {
            foreach ($validacion->errors()->all() as $error) {
                $errors[] = $error;
            }
        }
        else {
            $usuario = User::where('email', $request->email)->first();
            $success = true;
            $status = 200;
               
        }

        return response()->json([
                "success" => $success,
                "errors" => $errors,
                "status" => $status,
                "data" => $usuario
            ]);
        
      }


      
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Utils\FileUtils;
use Carbon\Carbon;
use Auth;
use App\RecursoObligacion;
use App\Obligacion;

class RecursoObligacionController extends Controller
{
    
    /**
     * Recurso Obligación: Crear
     * params: [api_token*, id_obligacion*, descripcion, archivo, status].
     * Método para la creación de un recurso de obligación.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function crear(Request $request) {
        $errors = [];
        $data = null;
        $reglas = [
            'id_obligacion' => 'required',
            'archivo' => 'required'
        ];
        $user = Auth::guard('api')->user();

        $validacion = Validator::make($request->all(), $reglas);

        if ($validacion->fails()) {
            foreach ($validacion->errors()->all() as $error) {
                $errors[] = $error;
            }
        } else {
            $archivo = $request->file('archivo');
            //Cargado de imagen de un boletín
            $rutaArchivo = FileUtils::guardar($request->file('archivo'), 'storage/robligacion/' . $user->id . '/');

            $request->request->add(['url' => $rutaArchivo]);
            $request->request->add(['fecha' => Carbon::now('America/Mexico_City')]);
            $robligacion = RecursoObligacion::create($request->all());
        
        }
        if (count($errors) > 0) {
            return response()->json([
                "success" => false,
                "errors" => $errors,
                "status" => 500,
                "data" => $robligacion
            ]);
        } else {
            return response()->json([
                "success" => true,
                "errors" => $errors,
                "status" => 200,
                "data" => $robligacion
            ]);
        }
    }


    /**
     * Recurso Obligación: Listado de recursos de obligación.
     * params: [id_obligacion*].
     * Ver todos los recursos de obligación dado el id de una obligación
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(Request $request) {
        $obligacion = Obligacion::with('recursosObligacion')->find($request->id_obligacion);
        return response()->json([
                "success" => true,
                "errors" => [],
                "status" => 200,
                "data" => $obligacion
            ]);
    }

    /**
     * Recurso Obligación: Actualizar
     * params: [id*, api_token*, descripcion, archivo, status].
     * Método para actualizar los datos de un recurso de obligación.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function actualizar(Request $request) {
        $data = null;
        $user = Auth::guard('api')->user();

        $robligacion = RecursoObligacion::find($request->id);
        if($robligacion) {
            if($request->file('archivo')) {
                //Cargado de imagen del boletín
                $rutaArchivo =  FileUtils::guardar($request->file('archivo'), 'storage/robligacion/' . $user->id . '/');
                FileUtils::eliminar($robligacion->url);
                $request->request->add(['url' => $rutaArchivo]);
            }
            $robligacion->update($request->all());
            $robligacion->save();
            
            return response()->json([
                "success" => true,
                "errors" => [],
                "status" => 200,
                "data" => $robligacion
            ]);
        }
        else {
            return response()->json([
                "success" => false,
                "errors" => ["Recurso de obligación no encontrado"],
                "status" => 404,
                "data" => null
            ]);
        }
        
    }

    /**
     * Recurso obligación: Eliminar
     * params: [api_token*, id*].
     * Método que elimina un recurso de obligación a partir de un id.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function eliminar(Request $request) {
        $robligacion = RecursoObligacion::find($request->id);
        if($robligacion) {
        $robligacion->delete();
            return response()->json([
                    "success" => true,
                    "errors" => [],
                    "status" => 200,
                    "data" => true
                ]);
        }
        else {
             return response()->json([
                    "success" => false,
                    "errors" => ["El recurso de obligación no fue encontrado"],
                    "status" => 200,
                    "data" => false
                ]);
        }
    }
}

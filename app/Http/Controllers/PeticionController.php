<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Validator;
use App\Peticion;
use Carbon\Carbon;

class PeticionController extends Controller
{
       
    /**
     * Petición: Crear
     * params: [api_token*, folio*, id_usuario*, descripcion*, fecha_inicio*, status].
     * Método para la creación de peticiones.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function crear(Request $request) {
        $errors = [];
        $data = null;
        $reglas = [
            'id_usuario' => 'required',
            'descripcion' => 'required',
            'fecha_inicio' => 'required',
            'folio' => 'required'
        ];

        $validacion = Validator::make($request->all(), $reglas);

        if ($validacion->fails()) {
            foreach ($validacion->errors()->all() as $error) {
                $errors[] = $error;
            }
        } else {
            $request->request->add(['fecha' => Carbon::now('America/Mexico_City')]);
            $peticion = Peticion::create($request->all());
        
        }
        if (count($errors) > 0) {
            return response()->json([
                "success" => false,
                "errors" => $errors,
                "status" => 500,
                "data" => null
            ]);
        } else {
            return response()->json([
                "success" => true,
                "errors" => $errors,
                "status" => 200,
                "data" => $peticion
            ]);
        }
    }


    /**
     * Peticion: Ver todas las peticiones
     * params: [api_token*].
     * Método que devuelve la lista de todas las peticiones
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(Request $request) {
        $peticiones = Peticion::with('user')->with('recursosPeticion')->get();
        return response()->json([
                "success" => true,
                "errors" => [],
                "status" => 200,
                "data" => $peticiones
            ]);
    }

    /**
     * Petición: Actualizar
     * params: [folio*, api_token*, descripcion, fecha_inicio, status].
     * Método para actualizar los datos de una petición.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function actualizar(Request $request) {
        $data = null;

        $peticion = Peticion::where('folio', $request->folio)->first();
        if($peticion) {
            $peticion->update($request->all());
            $peticion->save();
            
            return response()->json([
                "success" => true,
                "errors" => [],
                "status" => 200,
                "data" => $peticion
            ]);
        }
        else {
            return response()->json([
                "success" => false,
                "errors" => ["Petición no encontrada"],
                "status" => 404,
                "data" => null
            ]);
        }
        
    }

    /**
     * Petición: Eliminar
     * params: [api_token*, folio*].
     * Método que elimina una petición a partir de un id.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function eliminar(Request $request) {
        $peticion = Peticion::where('folio', $request->folio)->first();
        if($peticion) {
        $peticion->delete();
            return response()->json([
                    "success" => true,
                    "errors" => [],
                    "status" => 200,
                    "data" => true
                ]);
        }
        else {
             return response()->json([
                    "success" => false,
                    "errors" => ["La petición no fue encontrado"],
                    "status" => 200,
                    "data" => false
                ]);
        }
    }


    /**
     * Petición: Ver una petición acorde a un id enviado
     * params: [api_token*, folio*].
     * Método que devuelve la petición solicitado mediante su id.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function peticion(Request $request) {
        $peticion = Peticion::where('folio', $request->folio)->with('recursosPeticion')->first();
        if($peticion != null) {
            return response()->json([
            "success" => true,
            "errors" => [],
            "status" => 200,
            "data" => $peticion
            ]);
        }
        return response()->json([
            "success" => false,
            "errors" => ["La petición no fue encontrada"],
            "status" => 404,
            "data" => false
        ]);
    }


    /**
     * Petición: Peticiones por usuario
     * params: [api_token*].
     * Devuelve una lista de peticiones de acuerdo al api_token del usuario.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function peticiones(Request $request) {
        $usuario = Auth::guard('api')->user();
        $peticiones = $usuario->peticiones;
        return response()->json([
            "success" => true,
            "errors" => [],
            "status" => 200,
            "data" => $peticiones
        ]);
    }
}

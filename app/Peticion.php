<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $id_usuario
 * @property string $descripcion
 * @property string $fecha_inicio
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property User $user
 * @property RecursoPeticion[] $recursoPeticions
 */
class Peticion extends Model
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'peticion';

    /**
     * @var array
     */
    protected $fillable = ['id_usuario', 'descripcion', 'fecha_inicio', 'folio', 'status', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'id_usuario');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function recursosPeticion()
    {
        return $this->hasMany('App\RecursoPeticion', 'id_peticion');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $id_peticion
 * @property string $descripcion
 * @property string $fecha
 * @property boolean $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property Peticion $peticion
 */
class RecursoPeticion extends Model
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'recurso_peticion';

    /**
     * @var array
     */
    protected $fillable = ['id_peticion', 'descripcion', 'fecha', 'status', 'url', 
        'created_at', 'updated_at', 'deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function peticion()
    {
        return $this->belongsTo('App\Peticion', 'id_peticion');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $from
 * @property int $to
 * @property string $descripcion
 * @property string $fecha
 * @property string $created_at
 * @property string $updated_at
 */
class Mensaje extends Model
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'mensaje';

    /**
     * @var array
     */
    protected $fillable = ['from', 'to', 'descripcion', 'fecha', 'created_at', 'updated_at'];

}

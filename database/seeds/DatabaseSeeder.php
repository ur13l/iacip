<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ObligacionesSeeder::class);
        // $this->call(UsersTableSeeder::class);
    }
}

<?php
use Illuminate\Database\Seeder;

class ObligacionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('obligacion')->insert([
            'id' => 1,
            'nombre' => 'El marco normativo aplicable al sujeto obligado, en el que deberá incluirse leyes, códigos, reglamentos, decretos de creación, manuales administrativos, reglas de operación, criterios, políticas, entre otros.',
            'status' => '1',
            'url' => '1.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 2,
            'nombre' => 'Su estructura orgánica completa, en un formato que permita vincular cada parte de la estructura, las atribuciones y responsabilidades que le corresponden a cada servidor público, prestador de servicios profesionales o miembro de los sujetos obligados, de conformidad con las disposiciones aplicables',
            'status' => '1',
            'url' => '2.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 3,
            'nombre' => 'Las facultades de cada Área',
            'status' => '1',
            'url' => '3.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 4,
            'nombre' => 'Las metas y objetivos de las Áreas de conformidad con sus programas operativos',
            'status' => '1',
            'url' => '4.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 5,
            'nombre' => 'Los indicadores realacionados con temas de interés público o transcendencia social que conforme a sus funciones, deban establecer',
            'status' => '1',
            'url' => '5.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 6,
            'nombre' => 'Indicadores de gestión, metas y objetivos de los programas',
            'status' => '1',
            'url' => '6.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 7,
            'nombre' => 'El directorio de todos los Servidores Públicos, a partir del nivel de jefatura de departamento o su equivalente, o de menor nivel, cuando se brinde atención al público; manejen o apliquen recursos públicos; realicen actos de autoridad o presten servicios profesionales bajo el régimen de confianza u honorarios y personal de base.',
            'status' => '1',
            'url' => '7.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 8,
            'nombre' => 'La remuneración bruta y neta de todos los Servidores Públicos de base o de confianza, de todas las percepciones, incluyendo sueldos, prestaciones, gratificaciones, primas, comisiones, dietas, bonos, estímulos, ingresos y sistemas de compensación, señalando la periodicidad de dicha remuneración',
            'status' => '1',
            'url' => '8.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 9,
            'nombre' => 'Los gastos de representación y viáticos, así como el objeto e informe de comisión correspondiente',
            'status' => '1',
            'url' => '9.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 10,
            'nombre' => 'El número total de las plazas y del personal de base y confianza, especificando el total de las vacantes, por nivel de puesto, para cada unidad administrativa',
            'status' => '1',
            'url' => '10.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 11,
            'nombre' => 'Las contrataciones de servicios profesionales por honorarios',
            'status' => '1',
            'url' => '11.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 12,
            'nombre' => 'La información en Versión Pública de las declaraciones patrimoniales de los Servidores Públicos que así lo determinen, en los sistemas habilitados para ello, de acuerdo a la normatividad aplicable.',
            'status' => '1',
            'url' => '12.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 13,
            'nombre' => 'El domicilio de la Unidad de Transparencia, además de la dirección electrónica donde podrán recibirse las solicitudes para obtener la información',
            'status' => '1',
            'url' => '13.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 14,
            'nombre' => 'Las convocatorias a concursos para ocupar cargos públicos y los resultados de los mismos',
            'status' => '1',
            'url' => '14.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 15,
            'nombre' => 'La información de los programas de subsidios, estímulos y apoyos, en el que se deberá informar respecto de los programas de transferencia, de servicios, de infraestructura social y de subsidio.',
            'status' => '1',
            'url' => '15.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 16,
            'nombre' => 'Las condiciones generales de trabajo, contratos o convenios que regulen las relaciones laborales del personal de base o de confianza, así como los recursos públicos económicos, en especie o donativos, que sean entregados a los sindicatos y ejerzan como recursos públicos',
            'status' => '1',
            'url' => '16.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 17,
            'nombre' => 'La información curricular, desde el nivel de jefatura de departamento o equivalente, hasta el titular del sujeto obligado, así como, en su caso, las sanciones administrativas de que haya sido objeto',
            'status' => '1',
            'url' => '17.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 18,
            'nombre' => 'El listado de los Servidores Públicos con sanciones administrativas definitivas, especificando la causa de sanción y la disposición',
            'status' => '1',
            'url' => '18.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 19,
            'nombre' => 'Los servicios que ofrecen señalando los requisitos para acceder a ellos',
            'status' => '1',
            'url' => '19.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 20,
            'nombre' => 'Los trámites, requisitos y formatos que ofrecen',
            'status' => '1',
            'url' => '20.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 21,
            'nombre' => 'La información financiera sobre el presupuesto asignado, así como los informes del ejercicio trimestral del gasto, en términos de la Ley General de Contabilidad Gubernamental y demás normatividad aplicable',
            'status' => '1',
            'url' => '21.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 22,
            'nombre' => 'La información relativa a la deuda pública, en términos de la normatividad aplicable',
            'status' => '1',
            'url' => '22.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 23,
            'nombre' => 'Los montos destinados a gastos relativos a comunicación social y publicidad oficial desglosada por tipo de medio, proveedores, número de contrato o campaña',
            'status' => '1',
            'url' => '23.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 24,
            'nombre' => 'Resultados finales de auditorías',
            'status' => '1',
            'url' => '24.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 25,
            'nombre' => 'El resultado de la dictaminación de los estados financieros',
            'status' => '1',
            'url' => '25.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 26,
            'nombre' => 'Los montos, criterios, convocatorias y listado de personas físicas o morales a quienes, por cualquier motivo, se les asigne o permita usar recursos públicos o, en los términos de las disposiciones aplicables, realicen actos de autoridad. Asimismo, los informes que dichas personas les entreguen sobre el uso y destino de dichos recursos.',
            'status' => '1',
            'url' => '26.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 27,
            'nombre' => 'Las concesiones, contratos, convenios, permisos, licencias o autorizaciones otorgados, especificando los titulares de aquéllos, debiendo publicarse su objeto, nombre o razón social del titular, vigencia, tipo, términos, condiciones, monto y modificaciones, así como si el procedimiento involucra el aprovechamiento de bienes, servicios y recursos públicos',
            'status' => '1',
            'url' => '27.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 28,
            'nombre' => 'La información sobre los resultados sobre procedimientos de adjudicación directa, invitación restringida y licitación de cualquier naturaleza, incluyendo la Versión Pública del Expediente respectivo y de los contratos celebrados',
            'status' => '1',
            'url' => '28.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 29,
            'nombre' => 'Los informes que por disposición legal generen los sujetos obligados',
            'status' => '1',
            'url' => '29.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 30,
            'nombre' => 'Estadísticas',
            'status' => '1',
            'url' => '30.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 31,
            'nombre' => 'Informe de avances programáticos o presupuestales, balances generales y su estado financiero',
            'status' => '1',
            'url' => '31.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 32,
            'nombre' => 'Padrón de proveedores y contratistas',
            'status' => '1',
            'url' => '32.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 33,
            'nombre' => 'Los convenios de coordinación de concertación con los sectores social y privado',
            'status' => '1',
            'url' => '33.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 34,
            'nombre' => 'El inventario de bienes muebles e inmuebles en posesión y propiedad; así como de los que hayan sido dados de baja, su destino final y en su caso el ingreso que ello haya significado',
            'status' => '1',
            'url' => '34.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 35,
            'nombre' => 'Las recomendaciones emitidas por los órganos públicos del Estado mexicano u organismos internacionales garantes de los derechos humanos, así como las acciones que han llevado a cabo para su atención',
            'status' => '1',
            'url' => '35.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 36,
            'nombre' => 'Las resoluciones y laduos que se emitan en procesos o procedimientos seguidos en forma de juicio',
            'status' => '1',
            'url' => '36.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 37,
            'nombre' => 'Los mecanismos de participación ciudadana',
            'status' => '1',
            'url' => '37.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 38,
            'nombre' => 'Programas',
            'status' => '1',
            'url' => '38.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 39,
            'nombre' => 'Las actas y resoluciones del Comité de Transparencia',
            'status' => '1',
            'url' => '39.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 40,
            'nombre' => 'Todas las evaluaciones y encuestas que hagan los sujetos obligados a programas financiados con recursos públicos',
            'status' => '1',
            'url' => '40.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 41,
            'nombre' => 'Los estudios financiados con recursos públicos',
            'status' => '1',
            'url' => '41.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 42,
            'nombre' => 'El listado de personas jubiladas y pensionadas y el monto que reciben',
            'status' => '1',
            'url' => '42.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 43,
            'nombre' => 'Los ingresos recibidos por cualquier concepto señalando el nombre de las personas responsables de recibirlos, administrarlos y ejercerlos, así como su destino, indicando el destino de cada uno de ellos',
            'status' => '1',
            'url' => '43.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 44,
            'nombre' => 'Donaciones hechas a terceras personas en dinero o en especie',
            'status' => '1',
            'url' => '44.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 45,
            'nombre' => 'Catálogo de disposición y guía de archivo documental:',
            'status' => '1',
            'url' => '45.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 46,
            'nombre' => 'Actas de sesiones',
            'status' => '1',
            'url' => '46.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 47,
            'nombre' => 'Para efectos estadísticos, el listado de solicitudes a las empresas concesionarias de telecomunicaciones y proveedores de servicios o aplicaciones de Internet para la intervención de comunicaciones privadas, el acceso al registro de comunicaciones y la localización geográfica en tiempo real de equipos de comunicación, que contenga exclusivamente el objeto, el alcance temporal y los fundamentos legales del requerimiento, así como, en su caso, la mención de que cuenta con la autorización judicial correspondiente',
            'status' => '1',
            'url' => '47.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 48,
            'nombre' => 'Otra información',
            'status' => '1',
            'url' => '48.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 49,
            'nombre' => 'Relación de solicitudes de acceso a la información pública y sus respuestas',
            'status' => '1',
            'url' => '49.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 50,
            'nombre' => 'Las demás contenidas en esta ley',
            'status' => '1',
            'url' => '50.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 51,
            'nombre' => 'La relación de observaciones y resoluciones emitidas y el seguimiento a cada una de ellas, incluyendo las respuestas entregadas por los sujetos obligados a quienes sean los solicitantes en cumplimiento de las resoluciones',
            'status' => '1',
            'url' => '51.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 52,
            'nombre' => 'Criterios orientadores',
            'status' => '1',
            'url' => '52.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 53,
            'nombre' => 'Informes, actas y minutas de sesiones públicas de cuerpos colegiados',
            'status' => '1',
            'url' => '53.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 54,
            'nombre' => 'Resultados de evaluación a portales de transparencia de los sujetos obligados.',
            'status' => '1',
            'url' => '54.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 55,
            'nombre' => 'Estudios que apoyen resoluciones',
            'status' => '1',
            'url' => '55.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 56,
            'nombre' => 'Sentencias ejecutorias',
            'status' => '1',
            'url' => '56.png'
        ]);

        DB::table('obligacion')->insert([
            'id' => 57,
            'nombre' => 'Estadísticas de quejas',
            'status' => '1',
            'url' => '57.png'
        ]);

    }
}
<?php

use Illuminate\Database\Seeder;

class TipoUsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_usuario')->insert([
            'id' => 1,
            'descripcion' => 'Administrador'            
        ]);

        DB::table('tipo_usuario')->insert([
            'id' => 2,
            'descripcion' => 'Usuario'
        ]);
    }
}

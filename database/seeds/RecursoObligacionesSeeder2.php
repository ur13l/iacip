<?php

use Illuminate\Database\Seeder;

class RecursoObligacionesSeeder2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        	//FRACCION 23
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Erogación de recursos por contratación de servicios de impresión, difusión y publicidad trimestre 1 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/23/formato 23b 2017.xlsx',
                'id_obligacion' => '23', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);        
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Erogación de recursos por contratación de servicios de impresión, difusión y publicidad trimestre 2 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/23/XXIIIB Formato Erogacion de recursos por contratacion de servicios_trim_2_2017.xls',
                'id_obligacion' => '23', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);        
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Utilización de los Tiempos Oficiales: tiempo de Estado y tiempo fiscal trimestre 1 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/23/formato 23 c 2017.xlsx',
                'id_obligacion' => '23', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);        
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'XIIIA Formato Programa Anual de Comunicacion Social o equivalente 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/23/XXIIIA Formato Programa Anual de Comunicacion Social o equivalente_trim_1_2017.xls',
                'id_obligacion' => '23', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);        
    
            //FRACCION 24
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Auditorias trimestre 1 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/24/Auditorias_1.xlsx',
                'id_obligacion' => '24', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Auditorias trimestre 2 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/24/Auditorias_2.xlsx',
                'id_obligacion' => '24', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Auditorias trimestre 3 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/24/Auditorias_3.xlsx',
                'id_obligacion' => '24', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
            
            //FRACCION 25
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Formato Resultados de la dictaminación de los estados financieros 1er trim 17',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/25/Formato Resultados de la dictaminacion de los estados financieros 1er trim 17.xls',
                'id_obligacion' => '25', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Formato Resultados de la dictaminación de los estados financieros 2do trim 17',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/25/Formato Resultados de la dictaminacion de los estados financieros 2do trim 17.xls',
                'id_obligacion' => '25', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
            
            //FRACCION 26
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Metas 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/26/RecursosPublicos_1.xls',
                'id_obligacion' => '26', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Metas 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/26/XXVI_RecursosPublicos_trim_2_17.xls',
                'id_obligacion' => '26', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Metas 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/26/XXVI_RecursosPublicos_trim_3_17.xls',
                'id_obligacion' => '26', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
            
            //FRACCION 27
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Metas 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/27/27 Formato Las concesiones, contratos, convenios, permisos, licencias o autorizaciones otorgados.xlsx',
                'id_obligacion' => '27', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Metas 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/27/Fracción XXVII Contratos y Convenios.xls',
                'id_obligacion' => '27', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
            
            //FRACCION 28
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Resultados de procedimientos de adjudicación directa realizados trimestre 1 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/28/ADJUDICACION DIRECTA 1ER TRIM 2017.xlsx',
                'id_obligacion' => '28', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Resultados de procedimientos de licitación pública e invitación a cuando menos tres personas realizados por el Instituto de Acceso a la Información Pública para el Estado de Guanajuato trimestre 1 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/28/LicitacionPublica_1.xls',
                'id_obligacion' => '28', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Resultados de procedimientos de licitación pública e invitación a cuando menos tres personas realizados por el Instituto de Acceso a la Información Pública para el Estado de Guanajuato trimestre 2 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/28/Formato Resultados de procedimientos de licitacion publica e invitacion 2do trim 2017.xls',
                'id_obligacion' => '28', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Resultados de procedimientos de licitación pública e invitación a cuando menos tres personas realizados por el Instituto de Acceso a la Información Pública para el Estado de Guanajuato trimestre 3 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/28/Formato Resultados de procedimientos de licitacion publica e invitacion 3er trim 2017.xls',
                'id_obligacion' => '28', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
                    
            //FRACCION 29
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Informes trimestre 1 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/29/Formato29Informe2.xlsx',
                'id_obligacion' => '29', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);	
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Informes trimestre 2 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/29/XXIX Formato Informes emitidos_trim_2_2017.xls',
                'id_obligacion' => '29', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);	
    
            //FRACCION 30
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Formato XXX Estadísticas generadas trimestre 1 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/30/Formato XXX Estadísticas generadas.xls',
                'id_obligacion' => '30', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Formato XXX Estadísticas generadas trimestre 2 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/30/Formato XXX Estadisticas generadas_trim_2_2017.xls',
                'id_obligacion' => '30', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Formato XXX Estadísticas generadas trimestre 3 de 20177',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/30/Formato XXX Estadisticas generadas_trim_3_2017.xls',
                'id_obligacion' => '30', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
    
            //FRACCION 31
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Informes programáticos presupuestales, balances generales y estados financieros trimestre 1 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/31/AvancePresupuestal_1.xls',
                'id_obligacion' => '31', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);		
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Informes programáticos presupuestales, balances generales y estados financieros trimestre 2 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/31/AvancePresupuestal_2.xls',
                'id_obligacion' => '31', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);	
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Informes programáticos presupuestales, balances generales y estados financieros trimestre 3 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/31/AvancePresupuestal_3.xls',
                'id_obligacion' => '31', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);	
            
            //FRACCION 32
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Padrón de proveedores y contratistas trimestre 1 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/32/PadronProveedores_1.xls',
                'id_obligacion' => '32', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Padrón de proveedores y contratistas trimestre 2 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/32/Formato Padron de proveedores y contratistas 2do trim 2017.xls',
                'id_obligacion' => '32', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Padrón de proveedores y contratistas trimestre 3 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/32/Formato Padron de proveedores y contratistas 3er trim 2017.xls',
                'id_obligacion' => '32', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
            
            //FRACCION 33
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Convenios con sector social y privado 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/33/Convenios_1_2017.xls',
                'id_obligacion' => '33', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
            
            //FRACCION 34
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Inventario de bienes inmuebles trimestre 1 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/34/Formato Inventario de bienes muebles 1er trim 2017.xls',
                'id_obligacion' => '34', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Inventario de bienes inmuebles trimestre 2 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/34/Formato Inventario de bienes muebles 2do trim 2017.xls',
                'id_obligacion' => '34', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Inventario de bienes inmuebles trimestre 3 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/34/Formato Inventario de bienes muebles 3er trim 2017.xls',
                'id_obligacion' => '34', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Datos del responsable inmobiliario semestre 1 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/34/Responsable Inmobiliario 1er sem 2017.xlsx',
                'id_obligacion' => '34', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Datos del responsable inmobiliario semestre 2 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/34/Responsable Inmobiliario 2do sem 2017.xlsx',
                'id_obligacion' => '34', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
            
    //FRACCION 35
            DB::table('recurso_obligacion')->insert([
                'descripcion' => '35 a Formato Recomendaciones emitidas por la Comisión Nacional de Derechos Humanos trimestre 1 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/35/35 a Formato Recomendaciones emitidas por la Comisión Nacional de Derechos Humanos.xlsx',
                'id_obligacion' => '35', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);		
            DB::table('recurso_obligacion')->insert([
                'descripcion' => '35 a Formato Recomendaciones emitidas por la Comisión Nacional de Derechos Humanos trimestre 2 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/35/35 a Formato Recomendaciones emitidas por la Comisión Nacional de Derechos Humanos2.xls',
                'id_obligacion' => '35', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);		
            DB::table('recurso_obligacion')->insert([
                'descripcion' => '35 b Formato Casos especiales emitidos por la CNDH u otros organismos trimestre 1 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/35/35 b Formato Casos especiales emitidos por la CNDH u otros organismos.xlsx',
                'id_obligacion' => '35', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);		
            DB::table('recurso_obligacion')->insert([
                'descripcion' => '35 b Formato Casos especiales emitidos por la CNDH u otros organismos trimestre 2 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/35/35 b Formato Casos especiales emitidos por la CNDH u otros organismos2.xls',
                'id_obligacion' => '35', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);		
            DB::table('recurso_obligacion')->insert([
                'descripcion' => '35 c Formato Recomendaciones emitidas por Organismos internacionales trimestre 1 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/35/35 c Formato Recomendaciones emitidas por Organismos internacionales.xlsx',
                'id_obligacion' => '35', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);		
            DB::table('recurso_obligacion')->insert([
                'descripcion' => '35 c Formato Recomendaciones emitidas por Organismos internacionales trimestre 2 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/35/35 c Formato Recomendaciones emitidas por Organismos internacionales2.xls',
                'id_obligacion' => '35', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);		
    
            //FRACCION 36
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Resoluciones y laudos trimestre 1 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/36/ResolucionesLaudos_1.xls',
                'id_obligacion' => '36', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Resoluciones y laudos trimestre 2 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/36/ResolucionesLaudos_2.xls',
                'id_obligacion' => '36', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Resoluciones y laudos trimestre 3 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/36/ResolucionesLaudos_3.xls',
                'id_obligacion' => '36', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
            
            //FRACCION 37
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Mecanismos de participación ciudadana 2017
    ',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/37/XXXVII Formato Mecanismos de participacion ciudadana_trim_2_2017.xls',
                'id_obligacion' => '37', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
    
            //FRACCION 38
            
            
            //FRACCION 39
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Informe de Sesiones del Comité de Transparencia semestre 1 de 2017 (formato XXXIXa)',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/39/InformeSesionesComiteTransparencia2017.xlsx',
                'id_obligacion' => '39', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
    
        //FRACCION 40
        
        
        //FRACCION 41
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Estudios financiados trimestre 1 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/41/Estudios_1.xls',
                'id_obligacion' => '41', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Formato Estudios financiados con recursos publicos trimestre 2 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/41/XLIA_Formato Estudios financiados con recursos publicos.xls',
                'id_obligacion' => '41', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Formato Estudios elaborados en colaboracion con organizaciones del sector social, privado y personas fisicas trimestre 2 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/41/XLIB_Formato Estudios elaborados en colaboracion con organizaciones del sector social, privado y personas fisicas.xls',
                'id_obligacion' => '41', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Formato Estudios cuya elaboracion se contrato a organizaciones de los sectores social y privado trimestre 2 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/41/XLIC_Formato Estudios cuya elaboracion se contrato a organizaciones de los sectores social y privado.xls',
                'id_obligacion' => '41', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Formato Estudios, investigaciones o analisis elaborados fueron financiados por otras instituciones publica trimestre 2 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/41/XLID_Formato Estudios, investigaciones o analisis elaborados fueron financiados por otras instituciones publica.xls',
                'id_obligacion' => '41', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
    
            //FRACCION 42
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Listado de jubilados y pensionados trimestre 1 de 2017 (formato 42a)',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/42/ListadoPensionados_InstitutoSeguridadSocial_1.xls',
                'id_obligacion' => '42', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Listado de jubilados(as) y pensionados(as) y el monto que reciben trimestre 1 de 2017 (formato 42b)',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/42/JubiladosPensionados_1.xls',
                'id_obligacion' => '42', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Listado de jubilados(as) y pensionados(as) y el monto que reciben trimestre 2 de 2017 (formato 42b)',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/42/XLIIB_JubiladosPensionados_trim_2_17.xls',
                'id_obligacion' => '42', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Listado de jubilados(as) y pensionados(as) y el monto que reciben trimestre 3 de 2017 (formato 42b)',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/42/XLIIB_JubiladosPensionados_trim_3_17.xls',
                'id_obligacion' => '42', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
    
    
            //FRACCION 43
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Responsables de recibir, administrar y ejercer los ingresos trimestre 1 de 2017 (XLIIIB)',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/43/ResponsablesIngresos.xls',
                'id_obligacion' => '43', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);		
             DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Ingresos y su destino trimestre 1 de 2017 (XLIIIA)',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/43/Ingresos_1.xls',
                'id_obligacion' => '43', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);		
             DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Responsables de recibir, administrar y ejercer los ingresos trimestre 2 de 2017 (XLIIIB)',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/43/XLIIIB_ResponsablesIngresos_trim_2_17.xls',
                'id_obligacion' => '43', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);		
             DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Ingresos y su destino trimestre 2 de 2017 (XLIIIA)',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/43/XLIIIA_Ingresos_trim_2_2017.xls',
                'id_obligacion' => '43', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);		
             DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Responsables de recibir, administrar y ejercer los ingresos trimestre 3 de 2017 (XLIIIB)',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/43/XLIIIB_ResponsablesIngresos_trim_3_17.xls',
                'id_obligacion' => '43', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);		
             DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Ingresos y su destino trimestre 3 de 2017 (XLIIIA)',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/43/XLIIIA_Ingresos_trim_3_2017.xls',
                'id_obligacion' => '43', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);		
            
            //FRACCION 44
            
            //FRACCION 45
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Catálogo de disposición documental y guía simple de archivos de 2016-2017 (formato único)',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/45/CatalogoYGuiadeArchivos.xls',
                'id_obligacion' => '45', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Guía Simple 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/45/GUIA SIMPLE 2017.pdf',
                'id_obligacion' => '45', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
                DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Catálogo 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/45/CATALOGO 2017.doc',
                'id_obligacion' => '45', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
            
            //FRACCION 46
            
            //FRACCION 47
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Formato XLVII Solicitudes de Intervención de telecomunicaciones trimestre 1 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/47/Formato XLVII Listado de solicitudes a las empresas.xls',
                'id_obligacion' => '47', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Formato XLVII Solicitudes de Intervención de telecomunicaciones trimestre 2 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/47/Formato XLVII Listado de solicitudes a las empresas_trim_2_2017.xls',
                'id_obligacion' => '47', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Formato XLVII Solicitudes de Intervención de telecomunicaciones trimestre 3 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/47/Formato XLVII Listado de solicitudes a las empresas_trim_3_2017.xls',
                'id_obligacion' => '47', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
            
        
            //FRACCION 48
            
            //FRACCION 49
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'La relación de solicitudes de acceso a la información pública, así como las respuestas trimestre 1 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/49/FORMATO XLIX.xls',
                'id_obligacion' => '49', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'La relación de solicitudes de acceso a la información pública, así como las respuestas trimestre 2 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/49/FORMATO XLIX_trim_2_2017.xls',
                'id_obligacion' => '49', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
    
            //FRACCION 50
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Formato Las demás contenidas en esta ley y demás ordenamientos legales trimestre 2 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/50/Formato Las demás contenidas en esta ley y demás ordenamientos legales 2017.xls',
                'id_obligacion' => '50', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
    
            //FRACCION 51
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Relación de expedientes trimestre 1 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/51/RelacionExpedientes_1.xls',
                'id_obligacion' => '51', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Relación de expedientes trimestre 2 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/51/RelacionExpedientes_2.xlsx',
                'id_obligacion' => '51', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
    
            //FRACCION 52
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Criterios trimestre 1 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/52/CriteriosOrientadores_1_trim_2017.xlsx',
                'id_obligacion' => '52', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);		
            DB::table('recurso_obligacion')->insert([
                'descripcion' => 'Criterios trimestre 2 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/52/CriteriosOrientadores_2_trim_2017.xlsx',
                'id_obligacion' => '52', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);		
            
            //FRACCION 53
            
            //FRACCION 54
            DB::table('recurso_obligacion')->insert([
                'descripcion' => '31 IIID Formato Resultados de verificacion al cumplimiento de la Ley trimestre 1 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/54/31 III d Formato Resultados de verificacion al cumplimiento de la Ley.xls',
                'id_obligacion' => '54', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
             DB::table('recurso_obligacion')->insert([
                'descripcion' => '31 IIID Formato Resultados de verificacion al cumplimiento de la Ley trimestre 2 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/54/IIID_Formato Resultados de verificacion al cumplimiento de la Ley.xls',
                'id_obligacion' => '54', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
            
            //FRACCION 55
            
            //FRACCION 56
            DB::table('recurso_obligacion')->insert([
                'descripcion' => '31 III F Formato Listado de las sentencias, ejecutorias o suspensiones trimestre 1 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/56/31 III f Formato Listado de las sentencias, ejecutorias o suspensiones.xls',
                'id_obligacion' => '56', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
            
            //FRACCION 57
            DB::table('recurso_obligacion')->insert([
                'descripcion' => '31 III G Formato Quejas presentadas trimestre 1 de 2017',
                'url' => 'https://iacipapp.com/iacip/public/storage/robligacion/57/31 III g Formato Quejas presentadas.xls',
                'id_obligacion' => '57', //Relación con el id de la obligación en cuestión.
                'fecha' => '2017-10-23', //Relación con el id de la obligación en cuestión.
                'status' => '1',
            ]);
            
    }
}

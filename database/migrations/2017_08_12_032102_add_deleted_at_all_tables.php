<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeletedAtAllTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         //tabla de tipos de tipo_usuario
        Schema::table('tipo_usuario', function($table) {
            $table->softDeletes();
        });


        //tabla de usuarios
        Schema::table('users', function($table) {
            $table->softDeletes();
        });

        

        //tabla de petición
        Schema::table('peticion', function($table) {
            $table->softDeletes();
        });

        //tabla de recurso petición
        Schema::table('recurso_peticion', function($table) {
            $table->softDeletes();
        });


        //tabla de tipos de mensaje
        Schema::table('mensaje', function($table) {
            $table->softDeletes();
        });


        //tabla de obligaciones
        Schema::table('obligacion', function($table) {
            $table->softDeletes();
        });

        //tabla de recurso_obligacion
        Schema::table('recurso_obligacion', function($table) {
            $table->softDeletes();
        });


        //tabla de boletín
        Schema::table('boletin', function($table) {
            $table->softDeletes();
        });

        //tabla de novedades
        Schema::table('novedades', function($table) {
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
    

        //tabla de tipos de tipo_usuario
        Schema::create('tipo_usuario', function($table) {
            $table->increments('id');
            $table->string('descripcion', 200);
            $table->timestamps();
        });


        //tabla de usuarios
        Schema::table('users', function($table) {
            $table->integer('id_tipo')->unsigned();
            $table->foreign('id_tipo')->references('id')->on('tipo_usuario');
            $table->renameColumn('name', 'nombre');
            $table->string('apepat', 50);
            $table->string('apemat', 50);
            $table->string('telefono', 10);

        });

        

        //tabla de petición
        Schema::create('peticion', function($table) {
            $table->increments('id');
            $table->integer('id_usuario')->unsigned();
            $table->foreign('id_usuario')->references('id')->on('users');
            $table->string('descripcion', 200)->nullable();
            $table->dateTime('fecha_inicio');
            $table->string('status', 50);
            $table->timestamps();
        });

        //tabla de recurso petición
        Schema::create('recurso_peticion', function($table) {
            $table->increments('id');
            $table->string('descripcion', 200)->nullable();
            $table->dateTime('fecha');
            $table->boolean('status');
            $table->integer('id_peticion')->unsigned();
            $table->foreign('id_peticion')->references('id')->on('peticion');
            $table->timestamps();
        });


        //tabla de tipos de mensaje
        Schema::create('mensaje', function($table) {
            $table->increments('id');
            $table->integer('from');
            $table->integer('to');
            $table->string('descripcion', 200)->nullable();
            $table->dateTime('fecha');
            $table->timestamps();
        });


        //tabla de obligaciones
        Schema::create('obligacion', function($table) {
            $table->increments('id');
            $table->boolean('status');
            $table->string('url', 100)->nullable();
            $table->timestamps();
        });

        //tabla de recurso_obligacion
        Schema::create('recurso_obligacion', function($table) {
            $table->increments('id');
            $table->string('descripcion', 50);
            $table->string('url', 100)->nullable();
            $table->integer('id_obligacion')->unsigned();
            $table->foreign('id_obligacion')->references('id')->on('obligacion');
            $table->boolean('status');
            $table->dateTime('fecha');
            $table->timestamps();
        });


        //tabla de boletín
        Schema::create('boletin', function($table) {
            $table->increments('id');
            $table->dateTime('fecha');
            $table->string('titulo', 200);
            $table->string('descripcion',200)->nullable();
            $table->string('url_foto', 300);
            $table->boolean('status');
            $table->timestamps();
        });

        //tabla de novedades
        Schema::create('novedades', function($table) {
            $table->increments('id');
            $table->dateTime('fecha');
            $table->string('titulo', 200);
            $table->string('descripcion',200);
            $table->string('url_foto', 300);
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
